// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Thsi package is obsolete. Please use the [modernc.org/tk9.0] package instead.
//
// [modernc.org/tk9.0]: https://pkg.go.dev/modernc.org/tk9.0
package tk // import "modernc.org/tk"
