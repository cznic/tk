module modernc.org/tk

go 1.18

require (
	github.com/cosmos72/gls v0.0.0-20180519201422-29add83bde4c
	modernc.org/ccgo/v3 v3.16.13
	modernc.org/fontconfig v1.0.9
	modernc.org/libc v1.22.6
	modernc.org/strutil v1.1.3
	modernc.org/tcl v1.15.2
	modernc.org/x11 v1.0.14
	modernc.org/xft v1.0.8
)

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	golang.org/x/tools v0.0.0-20201124115921-2c860bdd6e78 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/cc/v3 v3.40.0 // indirect
	modernc.org/expat v1.0.9 // indirect
	modernc.org/freetype v1.0.7 // indirect
	modernc.org/gettext v0.0.14 // indirect
	modernc.org/httpfs v1.0.6 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.5.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/token v1.0.1 // indirect
	modernc.org/xau v1.0.13 // indirect
	modernc.org/xcb v1.0.13 // indirect
	modernc.org/xdmcp v1.0.14 // indirect
	modernc.org/xrender v1.0.7 // indirect
	modernc.org/z v1.7.3 // indirect
)
