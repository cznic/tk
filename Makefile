# Copyright 2021 The Tk-Go Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	all bench clean cover cpu editor internalError later mem nuke todo edit devbench

grep=--include=*.go
ngrep='TODOOK\|internalError\|testdata\|assets.go\|TODO-'


all: build_all_targets
	@LC_ALL=C date
	@go version 2>&1 | tee log
	@gofmt -l -s -w *.go | tee -a log
	@go install -v ./... | tee -a log
	@go vet 2>&1 | grep -v $(ngrep) || true
	@golint 2>&1 | grep -v $(ngrep) || true
	@nilness .
	@staticcheck | grep -v 'scanner\.go' || true
	@grep -n --color=always 'FAIL\|PASS' log || true
	LC_ALL=C date 2>&1 | tee -a log

generate:
	go generate 2>&1 | tee log-generate
	go build -v ./... 2>&1 | tee -a log-generate

widgets:
	go run ./internal/binder/main.go -v tk8.6.12/doc/ > widgets.go
	gofmt -l -s -w -r '(x) -> x'
	gofmt -l -s -w .
	gofmt -l -s -w .

build_all_targets:
	GOOS=darwin GOARCH=arm64 go build -v ./...
	GOOS=darwin GOARCH=arm64 go test -c -o /dev/null
	GOOS=linux GOARCH=386 go build -v ./...
	GOOS=linux GOARCH=386 go test -c -o /dev/null
	GOOS=linux GOARCH=amd64 go build -v ./...
	GOOS=linux GOARCH=amd64 go test -c -o /dev/null
	GOOS=linux GOARCH=arm go build -v ./...
	GOOS=linux GOARCH=arm go test -c -o /dev/null
	GOOS=linux GOARCH=arm64 go build -v ./...
	GOOS=linux GOARCH=arm64 go test -c -o /dev/null

darwin_arm64:
	@echo "Should be executed only on darwin/arm64."
	make generate

linux_amd64:
	@echo "Should be executed only on linux/amd64."
	make generate

linux_386_config:
	@echo "Should be executed only on linux/386."
	rm -rf tmp/
	mkdir -p tmp/config tmp/src
	GO_GENERATE_TMPDIR=tmp/src GO_GENERATE_SAVE_CONFIG=tmp/config go generate 2>&1 | tee log-generate

linux_386_pull:
	@echo "Can be executed everywhere"
	rm -rf tmp/
	mkdir tmp/
	rsync -rp nuc32:src/modernc.org/tk/tmp/ tmp/
	GO_GENERATE_TMPDIR=tmp/src GO_GENERATE_LOAD_CONFIG=tmp/config TARGET_GOOS=linux TARGET_GOARCH=386 go generate 2>&1 | tee log-generate
	go build -v ./... 2>&1 | tee -a log-generate

linux_arm_config:
	@echo "Should be executed only on linux/arm."
	rm -rf tmp/
	mkdir -p tmp/config tmp/src
	GO_GENERATE_TMPDIR=tmp/src GO_GENERATE_SAVE_CONFIG=tmp/config go generate 2>&1 | tee log-generate

linux_arm_pull:
	@echo "Can be executed everywhere"
	rm -rf tmp/
	mkdir tmp/
	rsync -rp pi32:src/modernc.org/tk/tmp/ tmp/
	GO_GENERATE_TMPDIR=tmp/src GO_GENERATE_LOAD_CONFIG=tmp/config TARGET_GOOS=linux TARGET_GOARCH=arm go generate 2>&1 | tee log-generate
	go build -v ./... 2>&1 | tee -a log-generate

linux_arm64_config:
	@echo "Should be executed only on linux/arm64."
	rm -rf tmp/
	mkdir -p tmp/config tmp/src
	GO_GENERATE_TMPDIR=tmp/src GO_GENERATE_SAVE_CONFIG=tmp/config go generate 2>&1 | tee log-generate

linux_arm64_pull:
	@echo "Can be executed everywhere"
	rm -rf tmp/
	mkdir tmp/
	rsync -rp pi64:src/modernc.org/tk/tmp/ tmp/
	GO_GENERATE_TMPDIR=tmp/src GO_GENERATE_LOAD_CONFIG=tmp/config TARGET_GOOS=linux TARGET_GOARCH=arm64 go generate 2>&1 | tee log-generate
	go build -v ./... 2>&1 | tee -a log-generate

bench:
	date 2>&1 | tee log-bench
	go test -timeout 24h -v -run '^[^E]' -bench . 2>&1 | tee -a log-bench
	grep -n 'FAIL\|SKIP' log-bench || true

clean:
	go clean
	rm -f *~ *.test *.out

cover:
	t=$(shell mktemp) ; go test -coverprofile $$t && go tool cover -html $$t && unlink $$t

cpu: clean
	go test -run @ -bench . -cpuprofile cpu.out
	go tool pprof -lines *.test cpu.out

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile *.go & fi

editor:
	gofmt -l -s -w *.go
	go test -c -o /dev/null 2>&1 | tee log-install
	go build -o /dev/null generator.go | tee -a log-install
	@gofmt -l -s -w .

later:
	@grep -n $(grep) LATER * || true
	@grep -n $(grep) MAYBE * || true

mem: clean
	go test -run @ -dev -bench . -memprofile mem.out -timeout 24h
	go tool pprof -lines -web -alloc_space *.test mem.out

nuke: clean
	go clean -i

todo:
	@grep -nr $(grep) ^[[:space:]]*_[[:space:]]*=[[:space:]][[:alpha:]][[:alnum:]]* * | grep -v $(ngrep) || true
	@grep -nrw $(grep) 'TODO\|panic' * | grep -v $(ngrep) || true
	@grep -nr $(grep) BUG * | grep -v $(ngrep) || true
	@grep -nr $(grep) [^[:alpha:]]println * | grep -v $(ngrep) || true
	@grep -nir $(grep) 'work.*progress' || true
