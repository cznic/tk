// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package tk // import "modernc.org/tk"

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strings"
	"testing"
	"time"

	"modernc.org/ccgo/v3/lib"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func stack() string { return string(debug.Stack()) }

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO, stack) //TODOOK
}

// ----------------------------------------------------------------------------

var (
	oInteractive = flag.Bool("interactive", false, "")
)

func TestMain(m *testing.M) {
	flag.Parse()
	rc := m.Run()
	os.Exit(rc)
}

func Test0(t *testing.T) {
	if !*oInteractive {
		t.Skip("!interactive")
	}

	const labelCaption = "Hello, World!"

	app, err := App()
	if err != nil {
		t.Fatal(err)
	}

	re, err := app.eval(fmt.Sprintf(`
label .hello -text %q
ttk::button .hello2 -text "Exit" -command {  destroy . }
pack .hello .hello2
.hello cget -text
`, labelCaption))
	if err != nil {
		t.Fatal(err)
	}

	if g, e := re, labelCaption; g != e {
		t.Fatalf("%q %q", g, e)
	}

	done := make(chan struct{})

	defer close(done)

	go func() {
		var i int
		ticker := time.NewTicker(500 * time.Millisecond)

		defer ticker.Stop()

		for range ticker.C {
			select {
			case <-done:
				return
			default:
				i++
				re, err := app.eval(fmt.Sprintf(`
.hello configure -text {%d}
.hello cget -text
`, i))
				if err != nil {
					panic(err)
				}

				if g, e := re, fmt.Sprint(i); g != e {
					panic(fmt.Errorf("%q %q", g, e))
				}
			}
		}
	}()

	if err := app.Main(); err != nil {
		t.Fatal(err)
	}
}

func Test1(t *testing.T) {
	if !*oInteractive {
		t.Skip("!interactive")
	}

	app := AppM()
	label := app.NewTLabelM().SetTextM("This is a label")
	button := app.NewTButtonM().SetTextM("Exit").
		SetCommandM(func(w Widget, args []string) error {
			app.DestroyM(app)
			return nil
		})
	app.PackM(label, button)

	done := make(chan struct{})

	defer close(done)

	go func() {
		ticker := time.NewTicker(500 * time.Millisecond)

		defer ticker.Stop()

		var i int
		for range ticker.C {
			select {
			case <-done:
				return
			default:
				i++
				s := fmt.Sprintf("%d", i)
				label.SetTextM(s)
				if g, e := label.TextM(), s; g != e {
					panic(fmt.Errorf("%q %q", g, e))
				}
			}
		}
	}()

	app.MainM()
}

func TestTk(t *testing.T) {
	notFile := []string{}
	skip := []string{
		// These tests fail in the C original tests.
		"bind-13.14",
		"canvText-20.1",
		"focus-2.6",
		"focus-2.7",
		"focus-2.9",
		"focus-5.1",
		"font-21.6",
		"font-4.9",
		"font-45.3",
		"font-8.4",
		"scrollbar-3.36",
		"scrollbar-3.38",
		"scrollbar-6.12.1",
		"scrollbar-6.29.1",
		"scrollbar-6.35",
		"scrollbar-6.44",
		"send-8.16",
		"text-27.15d",
		"textDisp-19.18",
		"textDisp-20.1",
		"textDisp-20.2",
		"textDisp-20.3",
		"textDisp-20.4",
		"textDisp-20.5",
		"unixEmbed-7.1",
		"unixEmbed-7.1a",
		"unixWm-4.1",
		"unixWm-4.2",
		"unixWm-4.3",
		"unixWm-44.7",
		"unixWm-44.8",
		"unixWm-45.2",
		"unixWm-45.4",
		"unixfont-1.2",
		"unixfont-2.2",
		"unixfont-2.3",
		"unixfont-2.4",
		"unixfont-2.6",
		"unixfont-2.8",
		"unixfont-2.9",
		"unixfont-5.12",
		"unixfont-5.8",
		"unixfont-5.9",
		"unixfont-8.4",
		"unixfont-8.6",
		"unixfont-9.1",
		"unixfont-9.2",
		"wm-stackorder-5.2",
		"wm-stackorder-5.3",

		//TODO
		"main-3.1",
		"textDisp-19.17",
	}
	tmpDir, err := ioutil.TempDir("", "tktest-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(tmpDir)

	bin := "tktest"
	if runtime.GOOS == "windows" {
		bin += ".exe"
	}
	binPath := filepath.Join(tmpDir, bin)
	if out, err := ccgo.Shell("go", "build", "-o", binPath, "modernc.org/tk/gotktest"); err != nil {
		t.Fatalf("%v\n%s", err, out)
	}

	os.Setenv("ERROR_ON_FAILURES", "1")
	if _, err := ccgo.Shell(
		binPath,
		filepath.Join("testdata", "tcl", "all.tcl"),
		"-geometry", "+0+0",
		"-notfile", strings.Join(notFile, " "),
		"-skip", strings.Join(skip, " "),
	); err != nil {
		t.Fatal(err)
	}
}

func TestTtk(t *testing.T) {
	skip := []string{}
	notFile := []string{}
	tmpDir, err := ioutil.TempDir("", "tktest-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(tmpDir)

	bin := "tktest"
	if runtime.GOOS == "windows" {
		bin += ".exe"
	}
	binPath := filepath.Join(tmpDir, bin)
	if out, err := ccgo.Shell("go", "build", "-o", binPath, "modernc.org/tk/gotktest"); err != nil {
		t.Fatalf("%v\n%s", err, out)
	}

	os.Setenv("ERROR_ON_FAILURES", "1")
	if _, err := ccgo.Shell(
		binPath,
		filepath.Join("testdata", "tcl", "ttk", "all.tcl"),
		"-geometry", "+0+0",
		"-notfile", strings.Join(notFile, " "),
		"-skip", strings.Join(skip, " "),
	); err != nil {
		t.Fatal(err)
	}
}
