// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strings"
	"testing"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func stack() string { return string(debug.Stack()) }

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO, stack) //TODOOK
}

// ----------------------------------------------------------------------------

var (
	oDump    = flag.Bool("dump", false, "")
	oDumpAll = flag.Bool("dumpall", false, "")

	docDir = filepath.Join("..", "..", "tk8.6.10", "doc")
)

func TestMain(m *testing.M) {
	flag.Parse()
	rc := m.Run()
	os.Exit(rc)
}

func TestParseN(t *testing.T) {
	s := filepath.Join(docDir, "*.n")
	m, err := filepath.Glob(s)
	if err != nil {
		t.Fatal(err)
	}

	var src, dump string
	for _, v := range m {
		b, err := ioutil.ReadFile(v)
		if err != nil {
			t.Fatal(v, err)
		}

		base := filepath.Base(v)
		nodes, err := ParseN(base, bytes.NewReader(b))
		if err != nil {
			t.Log(err)
			if *oDump {
				t.Logf("%v\n%s", src, dump)
			}
			t.Fatalf("%v: %v", base, err)
		}

		src = v
		dump = nodes.Dump()
		if *oDumpAll {
			t.Logf("%v\n%s", src, dump)
		}
	}
}

func TestGenerator(t *testing.T) {
	if err := main1(docDir, false, true); err != nil {
		t.Fatal(err)
	}
}
