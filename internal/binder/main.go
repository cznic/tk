// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"go/token"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"sort"
	"strings"

	"modernc.org/strutil"
)

// tk8.6.10
//
//	button.n		ttk_button.n
//	canvas.n
//	checkbutton.n		ttk_checkbutton.n
//	entry.n			ttk_entry.n
//	frame.n			ttk_frame.n
//	label.n			ttk_label.n
//	labelframe.n		ttk_labelframe.n
//	listbox.n
//	menu.n
//	menubutton.n		ttk_menubutton.n
//	message.n
//	panedwindow.n		ttk_panedwindow.n
//	radiobutton.n		ttk_radiobutton.n
//	scale.n			ttk_scale.n
//	scrollbar.n		ttk_scrollbar.n
//	spinbox.n		ttk_spinbox.n
//	text.n
//	toplevel.n
//	----------		-------------
//	18			12
//
//	ttk_button.n		button.n
//	ttk_checkbutton.n	checkbutton.n
//	ttk_combobox.n
//	ttk_entry.n		entry.n
//	ttk_frame.n             frame.n
//	ttk_label.n             label.n
//	ttk_labelframe.n        labelframe.n
//	ttk_menubutton.n	menubutton.n
//	ttk_notebook.n
//	ttk_panedwindow.n	panedwindow.n
//	ttk_progressbar.n       radiobutton.n
//	ttk_radiobutton.n       scale.n
//	ttk_scale.n             scrollbar.n
//	ttk_scrollbar.n         spinbox.n
//	ttk_separator.n
//	ttk_sizegrip.n
//	ttk_spinbox.n
//	ttk_treeview.n
//	--------------
//	18
//
//	Widget		Listed at https://tkdocs.com/widgets/index.html
//	Button		*
//	Canvas		*
//	Checkbutton	*
//	Combobox	*
//	Entry		*
//	Frame		*
//	Label		*
//	Labelframe	*
//	Listbox		*
//	Menu
//	Menubutton
//	Message
//	Notebook	*
//	Panedwindow	*
//	Progressbar	*
//	Radiobutton	*
//	Scale		*
//	Scrollbar	*
//	Separator	*
//	Sizegrip	*
//	Spinbox		*
//	Text		*
//	Toplevel
//	Treeview	*
//	--------
//	24

var (
	_ Node = (*Line)(nil)
	_ Node = (*Nodes)(nil)
	_ Node = (*SH)(nil)
	_ Node = (*SS)(nil)
	_ Node = (*SO)(nil)
	_ Node = node{}

	oVerbose = flag.Bool("v", false, "")

	baseList        []string
	nFiles          = map[string]*Nodes{} // *.n -> *Nodes
	widgetList      []*widget
	widgets         = map[string]*widget{} // *.n -> *widget
	standardOptions = newOptions(true)
)

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s\n\tTODO %s", origin(2), s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", r)
	os.Stderr.Sync()
	return r
}

type Node interface {
	Pos() token.Position
	getNode() node
}

type node struct {
	fn   string
	line int
	off  int
}

func (n node) getNode() node { return n }

// Pos implements Node.
func (n node) Pos() token.Position {
	return token.Position{Filename: n.fn, Line: n.line, Column: 1, Offset: n.off}
}

type Nodes struct {
	node
	List []Node
	Map  map[string][]Node
}

func newNodes(n node) *Nodes { return &Nodes{node: n, Map: map[string][]Node{}} }

func (n *Nodes) addNode(tag string, nd Node) {
	if nd == nil {
		return
	}

	if x, ok := nd.(*Nodes); ok && x == nil {
		return
	}

	if n.node.line == 0 {
		n.node = nd.getNode()
	}
	n.List = append(n.List, nd)
	n.Map[tag] = append(n.Map[tag], nd)
}

func (n *Nodes) Dump() string {
	var b bytes.Buffer
	f := strutil.IndentFormatter(&b, ".\t")
	n.dump(f)
	return b.String()
}

func (n *Nodes) dump(f strutil.Formatter) {
	m := map[Node]string{}
	for k, v := range n.Map {
		for _, w := range v {
			m[w] = k
		}
	}
	for _, v := range n.List {
		pos := v.Pos()
		pos.Filename = filepath.Base(pos.Filename)
		if tag := m[v]; tag != "" {
			f.Format("%s: ", tag)
		}
		f.Format("%s: ", pos)
		switch x := v.(type) {
		case *Nodes:
			f.Format("%i\n")
			x.dump(f)
			f.Format("%u")
		case *SH:
			f.Format("%s%i\n", x.Text)
			x.dump(f)
			f.Format("%u")
		case *Line:
			f.Format("%s\n", x.Text)
		case *SS:
			f.Format("%s%i\n", x.Text)
			x.dump(f)
			f.Format("%u")
		case *SO:
			f.Format("%s%i\n", x.Text)
			x.dump(f)
			f.Format("%u")
		case *OP:
			f.Format("%s%i\n", x.Text)
			x.dump(f)
			f.Format("%u")
		default:
			panic(todo("%T", x))
		}
	}
}

type parser struct {
	node
	b    strings.Builder
	err  error
	line string
	s    *bufio.Scanner
	tag  string

	valid bool
}

func newParser(name string, s *bufio.Scanner) (*parser, error) {
	r := &parser{node: node{name, 0, 1}, s: s}
	return r, r.next()
}

func (p *parser) next() error {
	p.valid = false
	_, _, err := p.tagLine()
	return err
}

func (p *parser) tagLine() (tag, line string, err error) {
	// defer func() { //TODO-
	// 	trc("line() %v: %q %q %v", p.Pos(), tag, line, err)
	// }()

	if p.valid {
		return p.tag, p.line, nil
	}

again:
	if line, err = p.line0(); err != nil {
		return "", "", err
	}

	if strings.HasPrefix(line, "'") || strings.HasPrefix(line, ".\"") {
		// comment
		goto again
	}

	p.valid = true
	switch {
	case strings.HasPrefix(line, "."):
		switch x := strings.IndexByte(line, ' '); {
		case x < 0:
			p.tag = line
			p.line = ""
		default:
			p.tag = line[:x]
			p.line = line[x+1:]
		}
	default:
		p.tag = ""
		p.line = line
	}
	return p.tag, p.line, nil
}

func (p *parser) line0() (line string, err error) {
	// defer func() { //TODO-
	// 	trc("line0() %v: %q %v", p.Pos(), line, err)
	// }()

	if p.err != nil {
		return "", p.err
	}

	if !p.s.Scan() {
		if p.err = p.s.Err(); p.err == nil {
			p.err = io.EOF
		}
		return "", p.err
	}

	p.node.line++
	p.b.Reset()
	var prev rune
	s := p.s.Text()
	p.off += len(s) + 1
	for _, v := range s {
		if prev == '\\' {
			prev = -1
			switch v {
			case
				'"',
				'&',
				'(',
				'-',
				'.',
				'\\',
				'|':

				p.b.WriteRune(v)
			case '0', 'e', 'f', 'w', 'N':
				p.b.WriteRune('\\')
				p.b.WriteRune(v)
			default:
				panic(todo("%v: %#U", p.Pos(), v))
			}
			continue
		}

		prev = v
		switch v {
		case '\\':
			// nop
		default:
			p.b.WriteRune(v)
		}
	}
	return p.b.String(), nil
}

func (p *parser) parse() (r *Nodes, err error) {
	r = newNodes(p.node)
	var n Node
	var tag, line string
	for err == nil {
		if tag, line, err = p.tagLine(); err != nil {
			return r, err
		}

		switch tag {
		case ".TH", ".so", ".PP", "", ".QW":
			err = p.next()
		case ".BS":
			n, err = p.parseBS()
			r.addNode(tag, n)
		case ".SH":
			n, err = p.parseSH()
			r.addNode(tag, n)
		case ".SO":
			n, err = p.parseSO()
			r.addNode(tag, n)
		default:
			panic(todo("%v: %q %q", p.Pos(), tag, line))
		}
	}
	return r, err
}

func (p *parser) parseBS() (r *Nodes, err error) {
	r = newNodes(p.node)
	var n Node
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case ".SH":
			n, err = p.parseSH()
			r.addNode(tag, n)
		case ".BE":
			return r, p.next()
		case ".SO":
			n, err = p.parseSO()
			r.addNode(tag, n)
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

type Line struct {
	node
	Tag  string
	Text string
}

type SH struct {
	*Nodes
	Text string
}

func (p *parser) parseSH() (r *SH, err error) {
	r = &SH{newNodes(p.node), p.line}
	var n Node
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case "", ".IP":
			r.addNode(tag, &Line{p.node, p.tag, p.line})
			err = p.next()
		case ".PP", ".QW", ".LP", ".PQ", ".nf", ".fi", ".sp", ".VS", ".VE", ".MT", ".br", ".":
			err = p.next()
		case ".SH", ".BE", ".SO":
			return r, err
		case ".CS":
			n, err = p.parseCS()
			r.addNode(tag, n)
		case ".SS":
			n, err = p.parseSS()
			r.addNode(tag, n)
		case ".RS":
			n, err = p.parseRS()
			r.addNode(tag, n)
		case ".TP":
			n, err = p.parseTP()
			r.addNode(tag, n)
		case ".OP":
			n, err = p.parseOP()
			r.addNode(tag, n)
		case ".DS":
			n, err = p.parseDS()
			r.addNode(tag, n)
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

type OP struct {
	*Nodes
	Text string
}

func (p *parser) parseOP() (r *OP, err error) {
	r = &OP{newNodes(p.node), p.line}
	var n Node
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case "", ".QW":
			r.addNode(tag, &Line{p.node, p.tag, p.line})
			err = p.next()
		case ".VS", ".VE":
			err = p.next()
		case ".OP", ".BE", ".SH":
			return r, err
		case ".RS":
			n, err = p.parseRS()
			r.addNode(tag, n)
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

func (p *parser) parseTP() (r *Line, err error) {
	r = &Line{p.node, p.tag, p.line}
	if p.line != "" && !isDigit(p.line[0]) {
		panic(todo("%v: %q %q (%v:)", p.Pos(), p.tag, p.line, origin(2)))
		return r, p.next()
	}

	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case "":
			r.Text = p.line
			return r, p.next()
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

func isDigit(c byte) bool { return c >= '0' && c <= '9' }

type SO struct {
	*Nodes
	Text string
}

func (p *parser) parseSO() (r *SO, err error) {
	r = &SO{newNodes(p.node), p.line}
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case "":
			r.addNode(tag, &Line{p.node, p.tag, p.line})
			err = p.next()
		case ".SE":
			return r, p.next()
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

type SS struct {
	*Nodes
	Text string
}

func (p *parser) parseSS() (r *SS, err error) {
	r = &SS{newNodes(p.node), p.line}
	var n Node
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case ".PP", "", ".QW", ".PQ", ".VS", ".VE", ".LP", ".":
			err = p.next()
		case ".DS":
			n, err = p.parseDS()
			r.addNode(tag, n)
		case ".SS", ".SH":
			return r, nil
		case ".IP":
			r.addNode(tag, &Line{p.node, p.tag, p.line})
			err = p.next()
		case ".RS":
			n, err = p.parseRS()
			r.addNode(tag, n)
		case ".CS":
			n, err = p.parseCS()
			r.addNode(tag, n)
		case ".TP":
			n, err = p.parseTP()
			r.addNode(tag, n)
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

func (p *parser) parseRS() (r *Nodes, err error) {
	r0 := newNodes(p.node)
	var n Node
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case ".PP", "", ".QW", ".PQ", ".LP", ".VS", ".VE", ".":
			err = p.next()
		case ".RE":
			return r, p.next()
		case ".DS":
			n, err = p.parseDS()
			if r == nil {
				r = r0
			}
			r.addNode(tag, n)
		case ".IP":
			if r == nil {
				r = r0
			}
			r.addNode(tag, &Line{p.node, p.tag, p.line})
			err = p.next()
		case ".TP":
			if r == nil {
				r = r0
			}
			n, err = p.parseTP()
			r.addNode(tag, n)
		case ".CS":
			n, err = p.parseCS()
			if n != nil && r == nil {
				r = r0
			}
			r.addNode(tag, n)
		case ".RS":
			n, err = p.parseRS()
			if r == nil {
				r = r0
			}
			r.addNode(tag, n)
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

func (p *parser) parseDS() (r *Nodes, err error) {
	r = newNodes(p.node)
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return r, err
		}

		switch tag {
		case ".ta", ".PP", ".QW":
			err = p.next()
		case "":
			r.addNode(tag, &Line{p.node, p.tag, p.line})
			err = p.next()
		case ".DE":
			return r, p.next()
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return r, err
}

func (p *parser) parseCS() (r Node, err error) {
	for err = p.next(); err == nil; {
		tag, line, err := p.tagLine()
		if err != nil {
			return nil, err
		}

		switch tag {
		case "":
			err = p.next()
		case ".CE":
			return nil, p.next()
		default:
			panic(todo("%v: %q %q (%v:)", p.Pos(), tag, line, origin(2)))
		}
	}
	return nil, err
}

// ParseN parses .n files from tkX.Y.Z/doc.
func ParseN(name string, rd io.Reader) (r *Nodes, err error) {
	defer func() {
		if e := recover(); e != nil {
			if err = fmt.Errorf("%v", e); strings.Contains(err.Error(), "runtime error") {
				err = fmt.Errorf("%s\n%s", err, debug.Stack())
			}
			return
		}

		if err == io.EOF {
			err = nil
		}
	}()

	p, err := newParser(name, bufio.NewScanner(rd))
	if err != nil {
		return nil, err
	}

	return p.parse()
}

type options struct {
	list []*option
	m    map[string]*option

	nonThemed int
	themed    int

	acceptThemed bool
	sorted       bool
}

func newOptions(acceptThemed bool) *options {
	return &options{m: map[string]*option{}, acceptThemed: acceptThemed}
}

func (o *options) addOption(opt *option) {
	if opt.themed && !o.acceptThemed {
		panic(todo("internal error"))
	}

	if ex, ok := o.m[opt.nm]; ok {
		switch {
		case opt.themed:
			if !ex.themed {
				ex.themed = true
				o.themed++
			}
		default:
			if !ex.nonThemed {
				ex.nonThemed = true
				o.nonThemed++
			}
		}
		return
	}

	switch {
	case opt.themed:
		o.themed++
	default:
		o.nonThemed++
	}

	o.list = append(o.list, opt)
	o.m[opt.nm] = opt
	o.sorted = false
}

func (o *options) sort() {
	if !o.sorted {
		sort.Slice(o.list, func(i, j int) bool { return o.list[i].nm < o.list[j].nm })
		o.sorted = true
	}

}

type option struct {
	nm   string // sans the '-' prefix
	docs string

	nonThemed bool
	themed    bool
}

func newOption(nm string, themed bool) *option {
	return &option{nm: nm, nonThemed: !themed, themed: themed}
}

func (o *option) info() *optionInfo {
	if r := optionInfos[o.nm]; r != nil {
		return r
	}

	return &zeroOptionInfo
}

func (o *option) camelCase() string {
	if s, ok := camelCase[o.nm]; ok {
		return s
	}

	panic(todo("method %s", o.nm))
}

func (o *option) getterDocs(prefix string) string {
	s := o.docs
	if s == "" {
		return "//TODO(nodocs)"
	}

	if strings.HasPrefix(s, "Specifies ") {
		s = fmt.Sprintf("%s%s reports %s", prefix, o.camelCase(), s[len("Specifies "):])
	}
	return "// " + strings.ReplaceAll(s, "\n", "\n// ")
}

func (o *option) setterDocs(prefix string) string {
	s := o.docs
	if s == "" {
		return "//TODO(nodocs)"
	}

	if strings.HasPrefix(s, "Specifies ") {
		s = fmt.Sprintf("%s%s specifies %s", prefix, o.camelCase(), s[len("Specifies "):])
	}
	return "// " + strings.ReplaceAll(s, "\n", "\n// ")
}

type widget struct {
	baseName        string // *.n
	goType          string
	specificOptions *options
	standardOptions []string
	tkName          string

	themed bool
}

// eg " \"-abc\t"
func tidyRawOption(s string) string {
	s = strings.Replace(s, "\"", "", 1)
	s = strings.TrimSpace(s)
	if !strings.HasPrefix(s, "-") {
		panic(todo("%q", s))
	}

	s = s[1:] // remove leading '-'
	return s
}

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "expected one argument, path to the doc directory")
		os.Exit(1)
	}

	if err := main1(flag.Arg(0), *oVerbose, false); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func main1(docDir string, verbose, noOutput bool) (err error) {
	defer func() {
		if e := recover(); e != nil {
			if err = fmt.Errorf("%v", e); strings.Contains(err.Error(), "runtime error") {
				err = fmt.Errorf("%s\n%s", err, debug.Stack())
			}
		}
	}()

	if err := parseAll(docDir, verbose); err != nil {
		return err
	}

	if err := extractWidgets(verbose); err != nil {
		return err
	}

	if err := extractStandardOptionDocs(); err != nil {
		return err
	}

	if err := extractThemedStandardOptionDocs(); err != nil {
		return err
	}

	var wr io.Writer
	switch {
	case noOutput:
		wr = io.Discard
	default:
		b := bufio.NewWriter(os.Stdout)
		wr = b

		defer func() {
			if e := b.Flush(); e != nil && err == nil {
				err = fmt.Errorf("flushing output: %v", err)
			}
		}()
	}

	if err := genHeader(wr); err != nil {
		return err
	}

	if err := genWidgets(wr); err != nil {
		return err
	}

	return nil
}

func extractThemedStandardOptionDocs() error {
	const base = "ttk_widget.n"
	n := nFiles[base]
	if n == nil {
		return fmt.Errorf("file not found: %s", base)
	}

	shs := n.Map[".SH"]
	if shs == nil || len(shs) == 0 {
		return fmt.Errorf("extractThemedStandardOptionDocs: .SH not found")
	}

	for _, sh := range shs {
		for _, op := range sh.(*SH).Nodes.Map[".OP"] {
			op := op.(*OP)
			a := strings.Fields(op.Text)
			opt := tidyRawOption(a[0])
			o := standardOptions.m[opt]
			if o.docs == "" {
				o.docs = strings.TrimSpace(strings.Join(docLines(op.Nodes.List), "\n"))
			}
		}
	}
	return nil
}

func extractStandardOptionDocs() error {
	const base = "options.n"
	n := nFiles[base]
	if n == nil {
		return fmt.Errorf("file not found: %s", base)
	}

	sh := n.Map[".SH"]
	if sh == nil || len(sh) == 0 {
		return fmt.Errorf("extractStandardOptionDocs: .SH not found")
	}

	for _, op := range sh[0].(*SH).Map[".OP"] {
		op := op.(*OP)
		a := strings.Fields(op.Text)
		opt := tidyRawOption(a[0])
		standardOptions.m[opt].docs = strings.TrimSpace(strings.Join(docLines(op.Nodes.List), "\n"))
	}
	return nil
}

func genWidgets(wr io.Writer) error {
	for _, w := range widgetList {
		fmt.Fprintf(wr, `
// %s represents the Tk %s widget.
type %[1]s struct {
	Window
}
`, w.goType, w.tkName)

		fmt.Fprintf(wr, `
// New%[1]s returns a new %[1]s as a child of w or an error, if any.
func (w *Window) New%[1]s() (r *%[1]s, err error) {
	r = &%[1]s{}
	p := &r.Window
	w.initChild(p)
	if _, err := r.eval("%s " + r.path); err != nil {
		return nil, err
	}

	return r, nil
}

// New%[1]sM is like New%[1]s but panics on error.
func (w *Window) New%[1]sM() *%[1]s {
	r, err := w.New%[1]s()
	if err != nil {
		panic(err)
	}

	return r
}
`, w.goType, w.tkName)

		for _, nm := range w.standardOptions {
			opt := standardOptions.m[nm]
			if err := genOption(wr, w, opt); err != nil {
				return err
			}
		}
		w.specificOptions.sort()
		for _, opt := range w.specificOptions.list {
			if err := genOption(wr, w, opt); err != nil {
				return err
			}
		}
	}
	return nil
}

func genOption(wr io.Writer, w *widget, opt *option) error {
	switch opt.info().typ {
	case str:
		if err := genOptionGetterStr(wr, w, opt); err != nil {
			return err
		}

		return genOptionSetterStr(wr, w, opt)
	case boolean:
		if err := genOptionGetterBool(wr, w, opt); err != nil {
			return err
		}

		return genOptionSetterBool(wr, w, opt)
	case integer:
		if err := genOptionGetterInt(wr, w, opt); err != nil {
			return err
		}

		return genOptionSetterInt(wr, w, opt)
	case command:
		if err := genOptionGetterCommand(wr, w, opt); err != nil {
			return err
		}

		return genOptionSetterCommand(wr, w, opt)
	case float:
		if err := genOptionGetterFloat(wr, w, opt); err != nil {
			return err
		}

		return genOptionSetterFloat(wr, w, opt)
	default:
		panic(todo("", opt.nm, opt.info().typ))
	}
}

func genOptionSetterFloat(wr io.Writer, w *widget, opt *option) error {
	if opt.info().ro {
		return nil
	}

	fmt.Fprintf(wr, `
%s
func (w *%s) Set%s(value float64) error {
	_, err := w.eval(fmt.Sprintf("%%s configure -%s {%%f}", w.path, value))
	return err
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) Set%[3]sM(value float64) *%[2]s {
	if err := w.Set%[3]s(value); err != nil {
		panic(err)
	}

	return w
}
`, opt.setterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionGetterFloat(wr io.Writer, w *widget, opt *option) error {
	fmt.Fprintf(wr, `
%s
func (w *%s) %s() (float64, error) {
	return w.evalFloat(fmt.Sprintf("%%s cget -%s", w.path))
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) %sM() float64 {
	r, err := w.%[3]s()
	if err != nil {
		panic(err)
	}

	return r
}
`, opt.getterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionGetterInt(wr io.Writer, w *widget, opt *option) error {
	fmt.Fprintf(wr, `
%s
func (w *%s) %s() (int, error) {
	return w.evalInt(fmt.Sprintf("%%s cget -%s", w.path))
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) %sM() int {
	r, err := w.%[3]s()
	if err != nil {
		panic(err)
	}

	return r
}
`, opt.getterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionSetterInt(wr io.Writer, w *widget, opt *option) error {
	if opt.info().ro {
		return nil
	}

	fmt.Fprintf(wr, `
%s
func (w *%s) Set%s(value int) error {
	_, err := w.eval(fmt.Sprintf("%%s configure -%s {%%d}", w.path, value))
	return err
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) Set%[3]sM(value int) *%[2]s {
	if err := w.Set%[3]s(value); err != nil {
		panic(err)
	}

	return w
}
`, opt.setterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionSetterCommand(wr io.Writer, w *widget, opt *option) error {
	fmt.Fprintf(wr, `
%s
func (w *%s) Set%s(value Command) error {
	s, err := w.app.newCommand(w, value)
	if err != nil {
		return err
	}

	_, err = w.eval(fmt.Sprintf("%%s configure -%s {%%s}", w.path, s))
	return err
}

// Set%[3]sM is like %[3]s but panics on error
func (w *%[2]s) Set%[3]sM(value Command) *%[2]s {
	if err := w.Set%[3]s(value); err != nil {
		panic(err)
	}

	return w
}
`, opt.setterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionGetterCommand(wr io.Writer, w *widget, opt *option) error {
	if !opt.info().wo {
		panic(todo(""))
	}

	return nil
}

func genOptionSetterBool(wr io.Writer, w *widget, opt *option) error {
	if opt.info().ro {
		return nil
	}

	fmt.Fprintf(wr, `
%s
func (w *%s) Set%s(value bool) error {
	_, err := w.eval(fmt.Sprintf("%%s configure -%s {%%d}", w.path, bool2int(value)))
	return err
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) Set%[3]sM(value bool) *%[2]s {
	if err := w.Set%[3]s(value); err != nil {
		panic(err)
	}

	return w
}
`, opt.setterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionGetterBool(wr io.Writer, w *widget, opt *option) error {
	fmt.Fprintf(wr, `
%s
func (w *%s) %s() (bool, error) {
	return w.evalBool(fmt.Sprintf("%%s cget -%s", w.path))
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) %sM() bool {
	s, err := w.%[3]s()
	if err != nil {
		panic(err)
	}

	return s
}
`, opt.getterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionGetterStr(wr io.Writer, w *widget, opt *option) error {
	fmt.Fprintf(wr, `
%s
func (w *%s) %s() (string, error) {
	return w.eval(fmt.Sprintf("%%s cget -%s", w.path))
}

// %[3]sM is like %[3]s but panics on error.
func (w *%[2]s) %sM() string {
	s, err := w.%[3]s()
	if err != nil {
		panic(err)
	}

	return s
}
`, opt.getterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genOptionSetterStr(wr io.Writer, w *widget, opt *option) error {
	if opt.info().ro {
		return nil
	}

	fmt.Fprintf(wr, `
%s
func (w *%s) Set%s(value string) error {
	_, err := w.eval(fmt.Sprintf("%%s configure -%s {%%s}", w.path, value))
	return err
}

// Set%[3]sM is like %[3]s but panics on error
func (w *%[2]s) Set%sM(value string) *%[2]s {
	if err := w.Set%s(value); err != nil {
		panic(err)
	}

	return w
}
`, opt.getterDocs(""), w.goType, opt.camelCase(), opt.nm)
	return nil
}

func genHeader(w io.Writer) error {
	args := append([]string(nil), os.Args...)
	args[0] = filepath.Base(args[0])
	_, err := fmt.Fprintf(w, `// Code generated by %v, DO NOT EDIT.

package tk // import "modernc.org/tk"

import "fmt"
`, args)
	return err
}

func extractWidgets(verbose bool) error {
	for _, base := range baseList {
		n := nFiles[base]
		var sos, shs []Node
		var tkName string
		if bss := n.Map[".BS"]; len(bss) != 0 {
			if len(bss) != 1 {
				panic(todo(""))
			}

			bs := bss[0].(*Nodes)
			sos = bs.Map[".SO"]
			shs = bs.Map[".SH"]
			nameSH := shs[0].(*SH)
			if nameSH.Text != "NAME" {
				panic(todo("internal error"))
			}

			a := strings.Fields(nameSH.List[0].(*Line).Text)
			a[0] = strings.TrimRight(a[0], ",") // menu, -> menu
			tkName = a[0]
		}

		if len(sos) == 0 {
			if sos = n.Map[".SO"]; len(sos) == 0 {
				continue // not a widget man page
			}
		}

		var specificOptions *SH
		shs = append(shs, n.Map[".SH"]...)
		for _, v := range shs {
			if sh := v.(*SH); sh.Text == `"WIDGET-SPECIFIC OPTIONS"` {
				specificOptions = sh
				break
			}
		}

		w := &widget{
			baseName: base,
			tkName:   tkName,
		}
		w.themed = strings.HasPrefix(tkName, "ttk::")
		w.specificOptions = newOptions(w.themed)
		widgetList = append(widgetList, w)
		widgets[base] = w
		switch {
		case w.themed:
			w.goType = camelCase[w.tkName[len("ttk::"):]]
		default:
			w.goType = camelCase[w.tkName]
		}
		if w.goType == "" {
			panic(todo("", w.tkName))
		}
		if w.themed {
			w.goType = "T" + w.goType
		}
		for _, v := range sos {
			if err := extractStandardOptions(w, v.(*SO)); err != nil {
				return err
			}
		}

		if err := extractSpecificOptions(w, specificOptions); err != nil {
			return err
		}

		sort.Strings(w.standardOptions)
		w.specificOptions.sort()
	}

	sort.Slice(widgetList, func(i, j int) bool { return widgetList[i].goType < widgetList[j].goType })
	if verbose {
		standardOptions.sort()
		fmt.Fprintln(os.Stderr, "\nWidgets:")
		for _, w := range widgetList {
			fmt.Fprintln(os.Stderr, w.goType)
		}
		fmt.Fprintln(os.Stderr, "\nStandard options:")
		for _, opt := range standardOptions.list {
			fmt.Fprintf(os.Stderr, "-%-20s", opt.nm)
			if opt.nonThemed {
				fmt.Fprintf(os.Stderr, "\tnon themed")
			} else {
				fmt.Fprintf(os.Stderr, "\t          ")
			}
			if opt.themed {
				fmt.Fprintf(os.Stderr, "\tthemed")
			}
			fmt.Fprintln(os.Stderr)
		}
		fmt.Fprintf(os.Stderr, "\nwidgets: %d, std options: %d, themed std options: %d\n", len(widgets), standardOptions.nonThemed, standardOptions.themed)
	}
	return nil
}

func docLines(n []Node) (lines []string) {
	for _, v := range n {
		switch x := v.(type) {
		case *Line:
			switch x.Tag {
			case "":
				lines = append(lines, x.Text)
			case ".QW":
				lines = append(lines, fmt.Sprintf("TODO .QW(%s)", x.Text))
			case ".IP":
				lines = append(lines, fmt.Sprintf("TODO .IP(%s)", x.Text))
			default:
				panic(todo("%v: %q %q", x.Pos(), x.Tag, x.Text))
			}
		case *Nodes:
			lines = append(lines, docLines(x.List)...)
		default:
			panic(todo("%v: %T", x.Pos(), x))
		}
	}
	return lines
}

func extractSpecificOptions(w *widget, sh *SH) error {
	if sh == nil {
		return nil
	}

	ops := sh.Map[".OP"]
	for _, op := range ops {
		op := op.(*OP)
		nm := tidyRawOption(strings.Fields(op.Text)[0])
		opt := newOption(nm, w.themed)
		w.specificOptions.addOption(opt)
		opt.docs = strings.TrimSpace(strings.Join(docLines(op.Nodes.List), "\n"))
	}
	w.specificOptions.sort()
	return nil
}

func extractStandardOptions(w *widget, so *SO) error {
	for _, n := range so.Nodes.List {
		if err := extractStandardOptions2(w, n.(*Line).Text); err != nil {
			return err
		}
	}
	return nil
}

func extractStandardOptions2(w *widget, s string) error {
	a := strings.Split(s, "\t")
	for _, nm := range a {
		nm = tidyRawOption(nm)
		w.standardOptions = append(w.standardOptions, nm)
		standardOptions.addOption(newOption(nm, w.themed))
	}
	return nil
}

func parseAll(docDir string, verbose bool) error {
	s := filepath.Join(docDir, "*.n")
	m, err := filepath.Glob(s)
	if err != nil {
		return fmt.Errorf("glob(%q): %v", s, err)
	}

	if len(m) == 0 {
		return fmt.Errorf("%s: no *.n files found", docDir)
	}

	for _, v := range m {
		b, err := ioutil.ReadFile(v)
		if err != nil {
			return fmt.Errorf("read file %s: %v", v, err)
		}

		base := filepath.Base(v)
		n, err := ParseN(base, bytes.NewReader(b))
		if err != nil {
			return fmt.Errorf("parsing %v: %v", base, err)
		}

		baseList = append(baseList, base)
		nFiles[base] = n
	}
	if g, e := len(nFiles), len(m); g != e {
		panic(todo("internal error %d != %d", g, e))
	}

	if verbose {
		fmt.Fprintf(os.Stderr, "parsed files: %d\n", len(baseList))
	}
	return nil
}

var camelCase = map[string]string{
	"activebackground":         "ActiveBackground",
	"activeborderwidth":        "ActiveBorderWidth",
	"activeforeground":         "ActiveForeground",
	"activerelief":             "ActiveRelief",
	"activestyle":              "ActiveStyle",
	"anchor":                   "Anchor",
	"aspect":                   "Aspect",
	"autoseparators":           "AutoSeparators",
	"background":               "Background",
	"bigincrement":             "BigIncrement",
	"bitmap":                   "Bitmap",
	"blockcursor":              "BlockCursor",
	"borderwidth":              "BorderWidth",
	"button":                   "Button",
	"buttonbackground":         "ButtonBackground",
	"buttoncursor":             "ButtonCursor",
	"buttondownrelief":         "ButtonDownRelief",
	"buttonuprelief":           "ButtonuPRelief",
	"canvas":                   "Canvas",
	"checkbutton":              "CheckButton",
	"class":                    "Class",
	"closeenough":              "CloseEnough",
	"colormap":                 "Colormap",
	"columns":                  "Columns",
	"combobox":                 "ComboBox",
	"command":                  "Command",
	"compound":                 "Compound",
	"confine":                  "Confine",
	"container":                "Container",
	"cursor":                   "Cursor",
	"default":                  "Default",
	"digits":                   "Digits",
	"direction":                "Direction",
	"disabledbackground":       "DisabledBackground",
	"disabledforeground":       "DisabledForeground",
	"displaycolumns":           "DisplayColumns",
	"elementborderwidth":       "ElementBorderWidth",
	"endline":                  "EndLine",
	"entry":                    "Entry",
	"exportselection":          "ExportSelection",
	"font":                     "Font",
	"foreground":               "Foreground",
	"format":                   "Format",
	"frame":                    "Frame",
	"from":                     "From",
	"handlepad":                "HandlePad",
	"handlesize":               "HandleSize",
	"height":                   "Height",
	"highlightbackground":      "HighlightBackground",
	"highlightcolor":           "HighlightColor",
	"highlightthickness":       "HighlightThickness",
	"image":                    "Image",
	"inactiveselectbackground": "InactiveSelectBackground",
	"increment":                "Increment",
	"indicatoron":              "Indicatoron",
	"insertbackground":         "InsertBackground",
	"insertborderwidth":        "InsertBorderWidth",
	"insertofftime":            "InsertOffTime",
	"insertontime":             "InsertOnTime",
	"insertunfocussed":         "InsertUnfocussed",
	"insertwidth":              "InsertWidth",
	"invalidcommand":           "InvalidCommand",
	"jump":                     "Jump",
	"justify":                  "Justify",
	"label":                    "Label",
	"labelanchor":              "LabelAnchor",
	"labelframe":               "LabelFrame",
	"labelwidget":              "LabelWidget",
	"length":                   "Length",
	"listbox":                  "ListBox",
	"listvariable":             "ListVariable",
	"maximum":                  "Maximum",
	"maxundo":                  "MaxUndo",
	"menu":                     "Menu",
	"menubutton":               "MenuButton",
	"message":                  "Message",
	"mode":                     "Mode",
	"notebook":                 "Notebook",
	"offrelief":                "OffRelief",
	"offvalue":                 "OffValue",
	"onvalue":                  "OnValue",
	"opaqueresize":             "OpaqueResize",
	"orient":                   "Orient",
	"overrelief":               "OverRelief",
	"padding":                  "Padding",
	"padx":                     "PadX",
	"pady":                     "PadY",
	"panedwindow":              "PanedWindow",
	"phase":                    "Phase",
	"postcommand":              "PostCommand",
	"progressbar":              "ProgressBar",
	"proxybackground":          "ProxyBackground",
	"proxyborderwidth":         "ProxyBorderWidth",
	"proxyrelief":              "ProxyRelief",
	"radiobutton":              "RadioButton",
	"readonlybackground":       "ReadonlyBackground",
	"relief":                   "Relief",
	"repeatdelay":              "RepeatDelay",
	"repeatinterval":           "RepeatInterval",
	"resolution":               "Resolution",
	"sashcursor":               "SashCursor",
	"sashpad":                  "SashPad",
	"sashrelief":               "SashRelief",
	"sashwidth":                "SashWidth",
	"scale":                    "Scale",
	"screen":                   "Screen",
	"scrollbar":                "Scrollbar",
	"scrollregion":             "ScrollRegion",
	"selectbackground":         "SelectBackground",
	"selectborderwidth":        "SelectBorderWidth",
	"selectcolor":              "SelectColor",
	"selectforeground":         "SelectForeground",
	"selectimage":              "SelectImage",
	"selectmode":               "SelectMode",
	"separator":                "Separator",
	"setgrid":                  "SetGrid",
	"show":                     "Show",
	"showhandle":               "ShowHandle",
	"showvalue":                "ShowValue",
	"sizegrip":                 "SizeGrip",
	"sliderlength":             "SliderLength",
	"sliderrelief":             "SliderRelief",
	"spacing1":                 "Spacing1",
	"spacing2":                 "Spacing2",
	"spacing3":                 "Spacing3",
	"spinbox":                  "SpinBox",
	"startline":                "StartLine",
	"state":                    "State",
	"style":                    "Style",
	"tabs":                     "Tabs",
	"tabstyle":                 "TabStyle",
	"takefocus":                "TakeFocus",
	"tearoff":                  "TearOff",
	"tearoffcommand":           "TearOffCommand",
	"text":                     "Text",
	"textvariable":             "TextVariable",
	"tickinterval":             "TickInterval",
	"title":                    "Title",
	"to":                       "To",
	"toplevel":                 "TopLevel",
	"treeview":                 "TreeView",
	"tristateimage":            "TristateImage",
	"tristatevalue":            "TristateValue",
	"troughcolor":              "TroughColor",
	"type":                     "Type",
	"underline":                "Underline",
	"undo":                     "Undo",
	"use":                      "Use",
	"validate":                 "Validate",
	"validatecommand":          "ValidateCommand",
	"value":                    "Value",
	"values":                   "Values",
	"variable":                 "Variable",
	"visual":                   "Visual",
	"width":                    "Width",
	"wrap":                     "Wrap",
	"wraplength":               "WrapLength",
	"xscrollcommand":           "XScrollCommand",
	"xscrollincrement":         "XScrollIncrement",
	"yscrollcommand":           "YScrollCommand",
	"yscrollincrement":         "YScrollIncrement",
}

type typ int

const (
	str     typ = iota // string
	boolean            // bool
	integer            // int
	command            // Command
	float              // float64
)

type optionInfo = struct {
	typ typ

	ro bool
	wo bool
}

var (
	zeroOptionInfo optionInfo

	optionInfos = map[string]*optionInfo{
		"class":           {ro: true},
		"closeenough":     {typ: float},
		"command":         {typ: command, wo: true},
		"confine":         {typ: boolean},
		"exportselection": {typ: boolean},
		"insertofftime":   {typ: integer},
		"insertontime":    {typ: integer},
		"xscrollcommand":  {typ: command, wo: true},
		"yscrollcommand":  {typ: command, wo: true},
	}
)
