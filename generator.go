// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"modernc.org/ccgo/v3/lib"
)

// Sync with all_test.go.
const (
	tarFile = tarName + "-src.tar.gz"
	tarName = "tk8.6.12"
)

type supportedKey = struct{ os, arch string }

var (
	gcc       = ccgo.Env("GO_GENERATE_CC", ccgo.Env("CC", "gcc"))
	gxx       = ccgo.Env("GO_GENERATE_CXX", ccgo.Env("CXX", "g++"))
	goarch    = ccgo.Env("TARGET_GOARCH", runtime.GOARCH)
	goos      = ccgo.Env("TARGET_GOOS", runtime.GOOS)
	supported = map[supportedKey]struct{}{
		{"darwin", "arm64"}: {},
		{"linux", "386"}:    {},
		{"linux", "amd64"}:  {},
		{"linux", "arm"}:    {},
		{"linux", "arm64"}:  {},
	}
	loadConfig       = ccgo.Env("GO_GENERATE_LOAD_CONFIG", "")
	saveConfig       = ccgo.Env("GO_GENERATE_SAVE_CONFIG", "")
	tmpDir           = ccgo.Env("GO_GENERATE_TMPDIR", "")
	verboseCompiledb = ccgo.Env("GO_GENERATE_VERBOSE", "") == "1"
)

func main() {
	fmt.Printf("Running on %s/%s.\n", runtime.GOOS, runtime.GOARCH)
	if _, ok := supported[supportedKey{goos, goarch}]; !ok {
		ccgo.Fatalf(true, "unsupported target: %s/%s", goos, goarch)
	}

	ccgo.MustMkdirs(true,
		"lib",
		"internal/tktest",
		"internal/wish",
	)
	if tmpDir == "" {
		tmpDir = ccgo.MustTempDir(true, "", "go-generate-")
		defer os.RemoveAll(tmpDir)
	}
	mustEmptyDir("assests", nil)
	ccgo.MustUntarFile(true, filepath.Join(tmpDir), tarFile, nil)
	ccgo.MustCopyDir(true, "assets", filepath.Join(tmpDir, tarName, "library"), nil)
	cdb, err := filepath.Abs(filepath.Join(tmpDir, "cdb.json"))
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cc, err := exec.LookPath(gcc)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cxx, err := exec.LookPath(gxx)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	os.Setenv("CC", cc)
	os.Setenv("CXX", cxx)
	cfg := []string{
		"--enable-dll-unloading=no",
		"--enable-langinfo=no",
		"--enable-load=no",
		"--enable-shared=no",
		"--enable-xss=no", //TODO
		// "--enable-symbols=mem", //TODO-
	}
	make := "make"
	switch goos {
	case "darwin":
		make = "gmake"
		cfg = append(cfg, "--with-tcl=/opt/homebrew/Cellar/tcl-tk/8.6.12/lib")
		os.Setenv("CFLAGS", "-I/opt/homebrew/Cellar/tcl-tk/8.6.12/include")
	}
	if _, err := os.Stat(cdb); err != nil {
		if !os.IsNotExist(err) {
			ccgo.Fatal(true, err)
		}

		platformDir := "unix"
		ccgo.MustInDir(true, filepath.Join(tmpDir, tarName, platformDir), func() error {
			ccgo.MustShell(true, "./configure", cfg...)
			switch fmt.Sprintf("%s/%s", goos, goarch) {
			case "darwin/arm64":
				ccgo.Shell("sed", "-i", "", "s/ -mdynamic-no-pic//", "Makefile")
			}
			switch {
			case verboseCompiledb:
				ccgo.MustRun(true, "-verbose-compiledb", "-compiledb", cdb, make, "binaries", "tktest")
			default:
				ccgo.MustRun(true, "-compiledb", cdb, make, "binaries", "tktest")
			}
			return nil
		})
	}
	ccgo.MustRun(true,
		"-export-defines", "",
		"-export-enums", "",
		"-export-externs", "X",
		"-export-fields", "F",
		"-export-structs", "",
		"-export-typedefs", "",
		"-lmodernc.org/fontconfig/lib",
		"-lmodernc.org/tcl/lib",
		"-lmodernc.org/x11/lib",
		"-lmodernc.org/xft/lib",
		"-o", filepath.Join("lib", fmt.Sprintf("tk_%s_%s.go", goos, goarch)),
		"-pkgname", "tk",
		"-trace-translation-units",
		"--load-config", loadConfig,
		"-save-config", saveConfig,
		cdb, "libtk8.6.a",
	)
	ccgo.MustRun(true,
		"-export-defines", "",
		"-lmodernc.org/tcl/lib",
		"-lmodernc.org/tk/lib",
		"-nocapi",
		"-o", filepath.Join("internal", "wish", fmt.Sprintf("wish_%s_%s.go", goos, goarch)),
		"-pkgname", "wish",
		"-trace-translation-units",
		"--load-config", loadConfig,
		"-save-config", saveConfig,
		cdb, "wish",
	)
	ccgo.MustRun(true,
		"-export-defines", "",
		"-lmodernc.org/tcl/lib",
		"-lmodernc.org/tk/lib",
		"-lmodernc.org/x11/lib",
		"-nocapi",
		"-o", filepath.Join("internal", "tktest", fmt.Sprintf("tktest_%s_%s.go", goos, goarch)),
		"-pkgname", "tktest",
		"-trace-translation-units",
		"--load-config", loadConfig,
		"-save-config", saveConfig,
		cdb,
		"libtkstub8.6.a",
		"tkAppInit.o#1",
		"tkOldTest.o",
		"tkSquare.o",
		"tkTest.o",
	)
}

func mustEmptyDir(s string, keep map[string]struct{}) {
	if err := emptyDir(s, keep); err != nil {
		ccgo.Fatal(true, err)
	}
}

func emptyDir(s string, keep map[string]struct{}) error {
	m, err := filepath.Glob(filepath.FromSlash(s + "/*"))
	if err != nil {
		return err
	}

	for _, v := range m {
		fi, err := os.Stat(v)
		if err != nil {
			return err
		}

		switch {
		case fi.IsDir():
			if err = os.RemoveAll(v); err != nil {
				return err
			}
		default:
			if _, ok := keep[filepath.Base(v)]; ok {
				break
			}

			if err = os.Remove(v); err != nil {
				return err
			}
		}
	}
	return nil
}
