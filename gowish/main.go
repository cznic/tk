// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"runtime"

	"modernc.org/libc"
	"modernc.org/tcl"
	"modernc.org/tk/internal/lib"
	"modernc.org/tk/internal/wish"
)

const (
	tclLibrary = "TCL_LIBRARY"
	tkLibrary  = "TK_LIBRARY"
)

func main() {
	runtime.LockOSThread()
	if os.Getenv(tclLibrary) == "" {
		dir, err := ioutil.TempDir("", "gowish-")
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		if err := tcl.Library(dir); err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		libc.AtExit(func() { os.RemoveAll(dir) })
		os.Setenv(tclLibrary, dir)
	}
	if os.Getenv(tkLibrary) == "" {
		dir, err := ioutil.TempDir("", "gowish-")
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		if err := lib.Library(dir); err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		libc.AtExit(func() { os.RemoveAll(dir) })
		os.Setenv(tkLibrary, dir)
	}
	libc.Start(wish.Main)
}
